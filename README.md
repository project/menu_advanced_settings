CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This module allows you to configure the menu title and description fields.
Setting a length and a display in the form view.

By default, the title and description have a fixed length of 255 characters and
are displayed in the form as an input texfield, this module allows you to
customize the max length and change to textarea format.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/menu_advanced_settings


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-8 for
   further information.


CONFIGURATION
-------------

If you want to change the maximum title length or menu description, or want to
change the title or description field from input text to textarea, go to the
module configuration page: /admin/structure/menu_advanced_settings
