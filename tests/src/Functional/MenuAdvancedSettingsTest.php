<?php

namespace Drupal\Tests\menu_advanced_settings\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a class for menu advanced settings functional tests.
 *
 * @group menu_advanced_settings
 */
class MenuAdvancedSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'user',
    'path',
    'menu_ui',
    'menu_advanced_settings',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An users with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'view the administration theme',
      'administer menu',
      'link to any page',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests users access.
   */
  public function testsAdminUi() {
    // Module administration menu.
    $this->drupalGet('admin/structure/menu_advanced_settings');
    $this->assertSession()->statusCodeEquals(200);

    // Can alter title and description fields.
    $edit = [
      'menu_title_length' => 1000,
      'menu_title_field' => 'textfield',
      'menu_description_length' => 2000,
      'menu_description_field' => 'textarea',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save configuration'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertText('The configuration options have been saved.');

    // Menu UI shows correct field type.
    $this->drupalGet('admin/structure/menu/manage/tools/add');
    $this->assertSession()->statusCodeEquals(200);
    // Description field has changed from textfield to textarea.
    $this->assertNotEmpty($this
      ->xpath('//textarea[@id="edit-description-0-value"]'));

    // User can add long title.
    $edit = [
      'link[0][uri]' => '/',
      'title[0][value]' => $this->randomMachineName(500),
      'description[0][value]' => $this->randomMachineName(2000),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    // $this->assertText('The menu link has been saved.');
  }

  /**
   * Tests module uninstall.
   */
  public function testsUninstall() {
    \Drupal::service('module_installer')->uninstall(['menu_advanced_settings']);
    $this->drupalGet('admin/structure/menu_advanced_settings');
    $this->assertSession()->statusCodeEquals(404);
  }

}
