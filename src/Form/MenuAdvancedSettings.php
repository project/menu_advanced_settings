<?php

namespace Drupal\menu_advanced_settings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\menu_advanced_settings\Services\SchemaManagement;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure menu advanced settings for this site.
 */
class MenuAdvancedSettings extends ConfigFormBase {

  /**
   * The schema helper.
   *
   * @var \Drupal\menu_advanced_settings\Services\SchemaManagement
   */
  protected $schemaManagement;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_advanced_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['menu_advanced_settings.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('menu_advanced_settings.settings');

    $form['title'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Title'),
    ];
    $form['title']['menu_title_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title max length'),
      '#default_value' => $config->get('menu_title_length') ?: 255,
    ];
    $form['title']['menu_title_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Title field type'),
      '#options' => [
        'textfield' => 'Textfield',
        'textarea'  => 'Textarea',
      ],
      '#default_value' => $config->get('menu_title_field') ?: 'textfield',
    ];

    $form['description'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Description'),
    ];
    $form['description']['menu_description_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description max length'),
      '#default_value' => $config->get('menu_description_length') ?: 255,
    ];
    $form['description']['menu_description_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Description field type'),
      '#options' => [
        'textfield' => 'Textfield',
        'textarea'  => 'Textarea',
      ],
      '#default_value' => $config->get('menu_description_field') ?: 'textfield',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title_length = $form_state->getValue('menu_title_length');
    $description_length = $form_state->getValue('menu_description_length');
    $settings = $this->config('menu_advanced_settings.settings');

    if ($this->schemaManagement->setMaxLength('title', $title_length)) {
      $settings->set('menu_title_length', $title_length);
    }

    if ($this->schemaManagement->setMaxLength('description', $description_length)) {
      $settings->set('menu_description_length', $description_length);
    }

    $settings
      ->set('menu_title_field', $form_state->getValue('menu_title_field'))
      ->set('menu_description_field', $form_state->getValue('menu_description_field'))
      ->save();

    // Flush entity cache.
    \Drupal::entityTypeManager()
    ->getStorage('menu_link_content')
    ->resetCache();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(SchemaManagement $schema_management) {
    $this->schemaManagement = $schema_management;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('menu_advanced_settings.schema_management'),
    );
  }

}
