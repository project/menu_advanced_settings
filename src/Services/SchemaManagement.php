<?php

namespace Drupal\menu_advanced_settings\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements menu schema managment helper class.
 */
class SchemaManagement {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Alter schema with a new field max length.
   */
  public function setMaxLength(string $field_name, int $field_length = 255) {
    if ($this->validateMaxLength($field_name, $field_length)) {
      // Alter the schema.
      $this->dbChangeVarcharField($field_name, $field_length);

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Validates schema max length to ensure content is not trucated.
   */
  public function validateMaxLength(string $field_name, int $field_length = 255) {
    // The minimum length is 255.
    $field_length = $field_length > 255 ? $field_length : 255;
    $length_function = $this->getLengthFuntion();

    // Get the tables.
    $key_value = \Drupal::keyValue('entity.storage_schema.sql');
    $key_name = 'menu_link_content.field_schema_data.' . $field_name;
    $storage_schema = $key_value->get($key_name);
    foreach ($storage_schema as $table_name => $table_schema) {
      // Ensure content is not truncated.
      $query = $this->db->select($table_name, 'ml');
      $query->addField('ml', $field_name);
      $query->where("$length_function($field_name) > $field_length");
      $count = $query->countQuery()
        ->execute()
        ->fetchField();

      if ($count > 0) {
        $this->messenger->addError(t('Max length cannot be altered @table(@field):@length. Exists @count contents with greater length and cannot be truncated.', [
          '@table'  => $table_name,
          '@field'  => $field_name,
          '@length' => $field_length,
          '@count'  => $count,
        ]));

        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * The length funtion by database type.
   */
  private function getLengthFuntion() {
    $length_function = 'char_length';
    switch ($this->db->databaseType()) {
      case 'pgsql':
      case 'sqlite':
        $length_function = 'length';
        break;

      case 'sqlsrv':
        $length_function = 'len';
        break;

    }

    return $length_function;
  }

  /**
   * Change length of a varchar field with data, safe with entity-updates.
   *
   * This updates the storage schema, the database schema, and the last
   * installed schema.
   *
   * @param string $field_name
   *   The field name to change.
   * @param int $field_length
   *   The new length of the field, must be larger than the previous value.
   */
  private function dbChangeVarcharField($field_name, $field_length) {
    /** @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $schema_repository */
    $schema_repository = \Drupal::service('entity.last_installed_schema.repository');
    /** @var \Drupal\Core\Entity\EntityFieldManager $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $base_field_definitions = $entity_field_manager->getBaseFieldDefinitions('menu_link_content');
    $schema_repository->setLastInstalledFieldStorageDefinition($base_field_definitions[$field_name]);
    $field_storage_definitions = $schema_repository->getLastInstalledFieldStorageDefinitions('menu_link_content');

    // Update the serialized schema property.
    $rc = new \ReflectionClass($field_storage_definitions[$field_name]);
    $schema_property = $rc->getProperty('schema');
    $schema_property->setAccessible(TRUE);
    $schema = $schema_property->getValue($field_storage_definitions[$field_name]);
    if (!empty($schema['columns']['value']['type'])) {
      $schema['columns']['value']['length'] = $field_length;
      $schema_property->setValue($field_storage_definitions[$field_name], $schema);

      // Update the field definition in the last installed schema repository.
      $schema_repository->setLastInstalledFieldStorageDefinitions('menu_link_content', $field_storage_definitions);
    }

    // Update the storage schema.
    $key_value = \Drupal::keyValue('entity.storage_schema.sql');
    $key_name = 'menu_link_content.field_schema_data.' . $field_name;
    $storage_schema = $key_value->get($key_name);
    // Update all tables where the field is present.
    foreach ($storage_schema as &$table_schema) {
      if (!empty($table_schema['fields'][$field_name]['type'])) {
        $table_schema['fields'][$field_name]['length'] = $field_length;
      }
    }
    $key_value->set($key_name, $storage_schema);

    // Update the database tables where the field is part of.
    foreach ($storage_schema as $table_name => $table_schema) {
      $this->db->schema()->changeField($table_name, $field_name, $field_name, $table_schema['fields'][$field_name]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $db, MessengerInterface $messenger) {
    $this->db = $db;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger')
    );
  }

}
